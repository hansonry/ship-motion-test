extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _targetSpatial: Spatial
var _velocity: Vector3
var _maxSpeed: float
var _maxRotSpeed : float
var _serverTform : Transform
var _lastOrigin : Vector3
var _lastAngle : Quat
var _nextOrigin : Vector3
var _nextAngle : Quat
var _serverTimer : float
var _lerpTimer : float

# Called when the node enters the scene tree for the first time.
func _ready():
	_targetSpatial = null
	_velocity = Vector3(0, 0, 0)

	_maxSpeed = 20
	_maxRotSpeed = deg2rad(25)

	_serverTform = transform
	_lastOrigin = transform.origin
	_lastAngle = transform.basis.get_rotation_quat()
	_nextOrigin = _lastOrigin
	_nextAngle = _lastAngle
	_serverTimer = 0
	_lerpTimer = 0

func set_target(targetSpatial: Spatial):
	_targetSpatial = targetSpatial

func _integration(delta, shipAcceleration : Vector3, shipRotationalVelocity: Vector3):
	var acceleration = _serverTform.basis.xform(shipAcceleration)
	var rotationalVelocity = _serverTform.basis.xform(shipRotationalVelocity)

	
	_velocity = _velocity + acceleration * delta 
	_serverTform.origin = _serverTform.origin + _velocity * delta 
	
	var shipQuat = _serverTform.basis.get_rotation_quat()
	var quatScale = 0.5 * delta
	var rotationVelQuat = Quat(rotationalVelocity.x * quatScale,
							   rotationalVelocity.y * quatScale,
							   rotationalVelocity.z * quatScale,
							   0)
	shipQuat = shipQuat + rotationVelQuat * shipQuat
	shipQuat = shipQuat.normalized()
	_serverTform.basis = Basis(shipQuat)

func _compute_desired_velocity(targetVector : Vector3):
	var desiredVelocity = Vector3.ZERO
	var dist = targetVector.length()
	var slowDownSpeed = 0
	if dist >= 0.001:
		slowDownSpeed = _maxSpeed * (dist / 35);
		var speed = min(slowDownSpeed, _maxSpeed)
		desiredVelocity = targetVector * (speed / dist)
	print("Desired Velocity: ", desiredVelocity, "  Max/slow: ", _maxSpeed, "/", slowDownSpeed)
	return desiredVelocity
	
func _compute_desired_acceleration_from_velocity(delta, desiredVelocity: Vector3):
	var thisShipVelocity = _serverTform.basis.xform_inv(_velocity)
	var velDiff = desiredVelocity - thisShipVelocity
	return velDiff / delta

func _saturate(v, minValue, maxValue):
	if v < minValue:
		return minValue
	if v > maxValue:
		return maxValue
	return v

func _limmit_acceleration_based_on_orientation(desiredAcceleration: Vector3):
	var shipRefrencedLimmitedAcc = Vector3(_saturate(desiredAcceleration.x, -20, 20),
										   _saturate(desiredAcceleration.y, -5, 5),
										   _saturate(desiredAcceleration.z, -5, 5))
	return shipRefrencedLimmitedAcc
	

func _compute_target_vector_from_ship(targetPosition: Vector3):
	var diff = targetPosition - _serverTform.origin
	var targetShipVector = _serverTform.basis.xform_inv(diff)
	return targetShipVector

func _compute_heading(position: Vector3):
	var headingVector = Vector2(position.x, position.z)
	# Assuming ship is facing X+
	# I want right to be positive
	var heading = -headingVector.angle()
	var length = headingVector.length()
	var elevation = atan2(position.y, length)
	var orientation = Vector2(heading, elevation)
	return orientation

func _server_process(delta):
	var limmitedRotationalVelocity = Vector3.ZERO
	var limmitedAcceleration       = Vector3.ZERO
	if _targetSpatial != null:
		print("")
		var targetVector  = _compute_target_vector_from_ship(_targetSpatial.transform.origin)
		var targetHeading = _compute_heading(targetVector)
		
		#print("targetVector: ", targetVector)
		
		# Compute Linear Acceleration
		var desiredVelocity     = _compute_desired_velocity(targetVector)
		var desiredAcceleration = _compute_desired_acceleration_from_velocity(delta, desiredVelocity)
		limmitedAcceleration    = _limmit_acceleration_based_on_orientation(desiredAcceleration)
		#print("limmitedAcceleration: ", limmitedAcceleration)
		
		# Compute Rotational Acceleration
		if desiredVelocity.length_squared() >= 25:
			var velocityHeading               = _compute_heading(desiredVelocity)
			var headingMag = velocityHeading.length()
			var limmitedHeadingVelocity = Vector2.ZERO
			if headingMag < _maxRotSpeed:
				limmitedHeadingVelocity = velocityHeading
			elif headingMag >= 0.001:
				limmitedHeadingVelocity = velocityHeading * (_maxRotSpeed / headingMag)
			limmitedRotationalVelocity = Vector3(0, 
												 limmitedHeadingVelocity.x, 
												 limmitedHeadingVelocity.y)
	_integration(delta, limmitedAcceleration, limmitedRotationalVelocity)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Server Stuff
	_serverTimer += delta
	if _serverTimer >= 1:
		_server_process(1)
		_serverTimer = _serverTimer - 1
		
		# Server Update Recived
		_lastOrigin = _nextOrigin
		_lastAngle = _nextAngle
		_nextOrigin = _serverTform.origin
		_nextAngle = _serverTform.basis.get_rotation_quat()
		_lerpTimer = 0
		
	# Client Stuff
	var percent = _lerpTimer / 1;
	transform.origin = _lastOrigin.linear_interpolate(_nextOrigin, percent)
	var rot = _lastAngle.slerp(_nextAngle, percent)
	transform.basis = Basis(rot)
	
	
	_lerpTimer = _lerpTimer + delta
	
	
