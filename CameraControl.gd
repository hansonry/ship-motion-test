extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var ship : Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	ship = get_node("../Ship")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	transform.origin = ship.transform.origin
