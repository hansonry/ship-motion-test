extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var shipNode : Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	shipNode = get_node("Ship")
	pass # Replace with function body.

func notifyClicked(obj : Spatial):
	print("Clicked")
	shipNode.set_target(obj)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
