extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var managerNode

# Called when the node enters the scene tree for the first time.
func _ready():
	managerNode = get_node("..")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if (event is InputEventMouseButton and
		event.button_index == BUTTON_LEFT and
		event.pressed):
			managerNode.notifyClicked(self)
